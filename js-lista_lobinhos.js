const url = "https://lobinhos-api.herokuapp.com/wolves"
let exibidos = [];
let page = localStorage.getItem("page");
let len;

if(!page) page = 1;

let SLobinhos = document.querySelector(".show_lobinho");
SLobinhos.addEventListener("click", (e) => {
    let aux = e.target;
    while(aux.className != "container")aux = aux.parentNode;
    localStorage.setItem("wolf-id", aux.id);
    window.location.href = "show_lobinho.html";
});

let tudo = document.querySelector(".paginacao");

const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}


adicionad = (lobo, i) => {
    let container = document.createElement("div");
    container.className = "container";
    container.id = i;
    let dir = document.createElement("div");
    dir.className = "direita1";
    let esq = document.createElement("div");
    esq.className = "esquerda1";
    let div = document.createElement("div");
    div.className = "div";
    let icon = document.createElement("img");
    icon.className = "icon";
    icon.src = lobo.photo;
    div.appendChild(icon);
    esq.appendChild(div);
    container.appendChild(esq);

    let opcoes = document.createElement("div");
    opcoes.className = "opcoes";
    let status = document.createElement("div");
    status.className = "statusLobo";
    let h2 = document.createElement("h2");
    let p = document.createElement("p");
    h2.innerHTML = lobo.name;
    p.innerHTML = `Idade: ${lobo.age} anos`;
    status.appendChild(h2);
    status.appendChild(p);
    opcoes.appendChild(status);
    p = document.createElement("p");
    p.className = "adote";
    p.innerHTML = "Adotar";
    opcoes.appendChild(p);
    dir.appendChild(opcoes);
    p = document.createElement("p");
    p.className = "descricao";
    p.innerHTML = lobo.description;
    dir.appendChild(p);
    container.appendChild(dir);

    let show = document.querySelector(".show_lobinho");
    show.appendChild(container);
};

adicionae = (lobo, i) => {
    let container = document.createElement("div");
    container.className = "container";
    container.id = i;
    let dir = document.createElement("div");
    dir.className = "direita2";
    let esq = document.createElement("div");
    esq.className = "esquerda2";
    let div = document.createElement("div");
    div.className = "div";
    let icon = document.createElement("img");
    icon.className = "icon";
    icon.src = lobo.photo;
    div.appendChild(icon);
    dir.appendChild(div);


    let opcoes = document.createElement("div");
    opcoes.className = "opcoes";
    let status = document.createElement("div");
    status.className = "statusLobo";
    let h2 = document.createElement("h2");
    let p = document.createElement("p");
    h2.innerHTML = lobo.name;
    p.innerHTML = `Idade: ${lobo.age} anos`;
    status.appendChild(h2);
    status.appendChild(p);
    opcoes.appendChild(status);
    p = document.createElement("p");
    p.className = "adote";
    p.innerHTML = "Adotar";
    opcoes.appendChild(p);
    esq.appendChild(opcoes);
    p = document.createElement("p");
    p.className = "descricao";
    p.innerHTML = lobo.description;
    esq.appendChild(p);
    container.appendChild(esq);
    container.appendChild(dir);

    let show = document.querySelector(".show_lobinho");
    show.appendChild(container);
};




gera = (url) => {
    fetch(url)
    .then(parseRequestToJSON)
    .then((responseAsJson) => {
        exibidos = [];
        len = responseAsJson.length;
        for(let i = 0 + (page-1)*5; i<responseAsJson.length;i++){
            if(exibidos.length >= 5) i= responseAsJson.length;
            else {
                lobo = responseAsJson[i];
                if(i%2 == 0) adicionad(lobo, i);
                else adicionae(lobo,  i);
                exibidos.push(lobo);
            }
        }
        let pagina = document.createElement("div");
        pagina.className = "page";
        let esq = document.createElement("span");
        if(page == 1){ esq.className = "atual";
        esq.innerHTML = "1";}
        else esq.innerHTML = `${page-1}`;
        let meio = document.createElement("span");
        if((page == 1)) meio.innerHTML = "2";
        else {meio.innerHTML = `${page}`;
        meio.className = "atual";}
        let dir = document.createElement("span");
        if((page == 1)) dir.innerHTML = "3";
        else dir.innerHTML = `${page+1}`;
        let back = document.createElement("span");
        back.className = "back";
        let next = document.createElement("span");
        next.className = "next";
        back.innerHTML = "<";
        next.innerHTML = ">";
        pagina.appendChild(back);
        pagina.appendChild(esq);
        if(page == 1 && (page+1)*5<=responseAsJson.length || page*5<=responseAsJson.length) pagina.appendChild(meio);
        if(page == 1 && (page+2)*5<=responseAsJson.length || (page+1)*5<=responseAsJson.length) pagina.appendChild(dir);
        while (tudo.firstChild) {
            tudo.removeChild(tudo.firstChild);}
        tudo.appendChild(pagina);
    });
};

gera(url);
tudo.addEventListener("click", (e) => {
    e.preventDefault();
    if(e.target.className != 'atual'){
        if(e.target.className != 'back' && page > 1){
            page--;
            
        }
        else if(e.target.className != 'next' && page*5 < len){
            page++;
            
        }
        else page = e.target.innerHTML;
        window.location.reload(true); 
    }
});

