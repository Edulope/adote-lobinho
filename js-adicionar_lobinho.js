let idUrl = localStorage.getItem("wolf-id");
const url = `https://lobinhos-api.herokuapp.com/wolves/`
let lobo;



const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

fetch(url)
.then(parseRequestToJSON)
.then((responseAsJson) => {
        lobo = responseAsJson[idUrl];
        if ((url) => {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(url);
      }){
        const photo = lobo.photo;
        const hPhoto = document.getElementById("icon");
        hPhoto.src = photo;
      }
      else {
          alert("imagem nao encontrada");
      }
      const gId = document.getElementById("ID");
        gId.innerHTML = `ID:${idUrl}`
        const nome = document.getElementsByTagName("h1");
        nome[0].innerHTML = `Adote o(a) ${lobo.name}`;
})
.catch(() => {
    lobo = {

    };
    const nome = document.getElementsByTagName("h1");
    nome[0].innerHTML = "LOBINHO NAO ENCONTRADO :("
    const dados = document.querySelector(".dados");
    dados.remove();
    const bot = document.querySelector(".botao");
    bot.remove();
});






let botao = document.querySelector(".botao");
botao.addEventListener("click", (e) => {
    e.preventDefault();
    lobo.adoption = {
        "age": document.getElementById("idade").value,
        "name": document.getElementById("nome").value,
        "email": document.getElementById("e-mail").value,
        "wolf_id": idUrl,
    }
    fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(lobo),
        })
    const novo_lobo = {
        "adoption":  {
          "age": document.getElementById("idade").value,
          "name": document.getElementById("nome").value,
          "email": document.getElementById("e-mail").value,
          "wolf_id": idUrl,
        }
      }
    if(!(novo_lobo.adoption.age.value == "" || novo_lobo.adoption.name.value == "" || novo_lobo.adoption.email.value == "")){
        document.getElementById("idade").value = "";
        document.getElementById("nome").value = "";
        document.getElementById("e-mail").value = "";
        fetch(`https://lobinhos-api.herokuapp.com/adoptions`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(novo_lobo),
        })
        .catch(() => alert("nao foi possivel cadastrar o lobo"))
    }
    else alert("por favor, preencha todos os campos adequadamente");
});
