let idUrl = localStorage.getItem("wolf-id");
const url = `https://lobinhos-api.herokuapp.com/wolves/`

const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

fetch(url)
.then(parseRequestToJSON)
.then((responseAsJson) => {
        if ((url) => {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(url);
      }){
        const photo = responseAsJson[idUrl].photo;
        const hPhoto = document.getElementById("icon");
        hPhoto.src = photo;
      }
      else {
          alert("imagem nao encontrada");
      }
      const nome = document.getElementsByTagName("h1");
        nome[0].innerHTML = `Adote o(a) ${responseAsJson[idUrl].name}`;
        const desc = document.querySelector(".descricao");
        desc.innerHTML = responseAsJson[idUrl].description;
})
.catch(() => {
    const nome = document.getElementsByTagName("h1");
    nome[0].innerHTML = "LOBINHO NAO ENCONTRADO :("
    const desc = document.querySelector(".descricao");
    desc.innerHTML = "Infelizmente, nao foi possivel encontrar o lobinho";
    const esq = document.querySelector(".esquerda");
    esq.remove();
});

const del = document.querySelector(".delete");
del.addEventListener("click", (e) =>{
    e.preventDefault();
    fetch(url, {
        method: 'DELETE'  
    })
    .then(() => window.location.href = "lista_lobinhos.html")
    .catch(() => console.log("ERROR"))
});