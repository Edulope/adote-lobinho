const url = "https://lobinhos-api.herokuapp.com";

let botao = document.querySelector(".botao");
botao.addEventListener("click", (e) => {
    e.preventDefault();
    const novo_lobo = {"wolf": {
        "photo": document.getElementById("link").value,
        "age": document.getElementById("age").value,
        "name": document.getElementById("name").value,
        "description": document.getElementById("Mensagem").value,
      }}
    if(!(novo_lobo.wolf.photo == "" || novo_lobo.wolf.age == "" || novo_lobo.wolf.name == "" || novo_lobo.wolf.description == "")){
        document.getElementById("link").value = "";
        document.getElementById("age").value = "";
        document.getElementById("name").value = "";
        document.getElementById("Mensagem").value = "";
        fetch(`${url}/wolves`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(novo_lobo),
        })
        .then(() => window.location.href = "lista_lobinhos.html")
        .catch(() => alert("nao foi possivel cadastrar o lobo"))
    }
    else alert("por favor, preencha todos os campos adequadamente");
});